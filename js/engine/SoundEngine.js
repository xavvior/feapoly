class Sound {
    static DICE = "dice";
    static ELEVATOR = "elevator";
    static KOCC = "kocc";
    static STEPS = "dice";
    static SZISSZ = "szissz";
    static LENNE_ITT_EGY_LEHETOSEG = "tibi";
    static NAGYON_NEHEZ_EZ_A_FELADAT_FEL_ORA = "tibi-2";
    static TOPPRIO = "top_prio";
    static CSUSSZ = "csussz";

    static play(name) {
        const audio = new Audio(`sounds/${name}.m4a`);
        audio.play();
    }
}