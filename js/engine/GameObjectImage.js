class GameObjectImage extends GameObject {
    constructor(x, y, sizeX, sizeY, imageRef) {
        super(x, y, sizeX, sizeY);
        this.image = ImageHandler.get(imageRef);
    }

    render(ctx) {
        ctx.drawImage(this.image, this.x, this.y, this.sizeX, this.sizeY);
    }
}