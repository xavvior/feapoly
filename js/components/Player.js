class Player extends GameObjectImage {
    static NAMES = ['', 'QG', 'MetriX', 'CV', 'TN'];
    currentField = 0;

    constructor(id, x, y, sizeX, sizeY, src) {
        super(x, y, sizeX, sizeY, src);
        this.id = id;
        this.moveToFieldImmediately(0, false);
        this.currentField = 0;
    }

    render(ctx) {
        let x = this.x;
        let y = this.y;
        switch(this.id){
            case 1: x-=30; y-=30; break;
            case 2: x+=30; y-=30; break;
            case 3: x-=30; y+=30; break;
            case 4: x+=30; y+=30; break;
        }
        ctx.drawImage(this.image, x, y, this.sizeX, this.sizeY);
    }

    moveToField(fieldId) {
        this.moveRecursion(this.currentField+1, fieldId);
        this.currentField = fieldId;
    }

    moveToFieldImmediately(fieldId, smooth = true) {
        super.moveTo("PLAYER", GameMechanic.fields[fieldId], smooth);
    }

    moveRecursion(current, total) {
        if (current > total) {
            return null;
        }
        return this.moveTo("PLAYER", GameMechanic.fields[current], true, () => this.moveRecursion(current + 1, total));

    }
}