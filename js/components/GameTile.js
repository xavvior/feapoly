class GameTile extends BaseTile {
    static SIZE_A = 96;
    static SIZE_B = 128;

    constructor(x, y, sizeX, sizeY, src, id) {
        super(x, y, sizeX, sizeY, src, id);
    }
}