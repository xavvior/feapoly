class GameMechanic {
    static renderer = null;
    static descriptionField = null;
    static numberOfPlayers = null;
    static fields = [];
    static players = [];
    static activePlayer = 1;

    static init() {
        this.renderer = Renderer.getInstance(document.getElementById('monopolyCanvas'));
        ImageHandler.init();
        GameMechanic.initBoard();
        this.step_1();
    }

    static initBoard() {
        const CARD_WIDTH = GameTile.SIZE_A;
        const CARD_HEIGHT = GameTile.SIZE_B;

        for (let i = 0; i < 11; i++) {
            this.renderer.addElement(new GameTile(2 + i * (CARD_WIDTH + 2) + CARD_HEIGHT, 0, CARD_WIDTH, CARD_HEIGHT, `property/${21 + i}.png`, 21 + i));
        }
        for (let i = 0; i < 7; i++) {
            this.renderer.addElement(new GameTile(0, i * (CARD_WIDTH + 2) + CARD_HEIGHT, CARD_HEIGHT, CARD_WIDTH, `property/${19 - i}.png`, 19 - i));
        }
        for (let i = 0; i < 11; i++) {
            this.renderer.addElement(new GameTile(2 + i * (CARD_WIDTH + 2) + CARD_HEIGHT, 6.35 * CARD_HEIGHT, CARD_WIDTH, CARD_HEIGHT, `property/${11 - i}.png`, 11 - i));
        }
        for (let i = 0; i < 7; i++) {
            this.renderer.addElement(new GameTile(12.57 * CARD_WIDTH, i * (CARD_WIDTH + 2) + CARD_HEIGHT, CARD_HEIGHT, CARD_WIDTH, `property/${33 + i}.png`, 33 + i));
        }
        this.renderer.addElement(new CornerTile(9.44 * CARD_HEIGHT, 0, CARD_HEIGHT, CARD_HEIGHT, `property/${32}.png`, 32));
        this.renderer.addElement(new CornerTile(0 * CARD_HEIGHT, 0, CARD_HEIGHT, CARD_HEIGHT, `property/${20}.png`, 20));
        this.renderer.addElement(new CornerTile(9.44 * CARD_HEIGHT, 7 * (CARD_WIDTH + 2) + CARD_HEIGHT, CARD_HEIGHT, CARD_HEIGHT, `property/${0}.png`, 0));
        this.renderer.addElement(new CornerTile(0, 7 * (CARD_WIDTH + 2) + CARD_HEIGHT, CARD_HEIGHT, CARD_HEIGHT, `property/${12}.png`, 12));

        for (let i = 0; i < 40; i++) {
            const result = this.renderer.elements.find(a => a.id === i);
            this.fields.push(result);
        }

        this.descriptionField = new Description(670, 600);
        this.renderer.addElement(this.descriptionField);

        Die.dice.push(new Die(1, 1, 50, 50));
        Die.dice.push(new Die(1, 1, 50, 50));
        this.renderer.addElement(Die.dice[0]);
        this.renderer.addElement(Die.dice[1]);

        Die.diceStation = new InvisibleTile(650, 450, 100, 100);
        this.renderer.addElement(Die.diceStation);
        Die.dice[0].moveTo("DIE_MOVING", Die.diceStation);
        Die.dice[1].moveTo("DIE_MOVING", Die.diceStation);
    }

    static step_1() {
        const kde = (event) => {
            if (!['2', '3', '4'].includes(event.key)) {
                this.descriptionField.setMessage("Csak 2-től 4-ig nyomj meg egy gombot.");
                return;
            }
            this.numberOfPlayers = Number(event.key);

            this.players.push(new Player(1, 10, 10, 50, 50, ImageHandler.QG));
            if (this.numberOfPlayers >= 2) this.players.push(new Player(2, 10, 10, 50, 50, ImageHandler.METRX));
            if (this.numberOfPlayers >= 3) this.players.push(new Player(3, 10, 10, 50, 50, ImageHandler.CV));
            if (this.numberOfPlayers >= 4) this.players.push(new Player(4, 10, 10, 50, 50, ImageHandler.TN));

            console.log(this.numberOfPlayers);
            this.players.forEach(e => this.renderer.addElement(e));
            this.step_2();
            document.removeEventListener('keydown', kde);
        }
        this.descriptionField.setMessage("Üdvözlünk a FEAPolyban. Hányan játszatok? Nyomd meg a számot (2-4).");
        document.addEventListener('keydown', kde);
    }

    static step_2() {
        this.descriptionField.setMessage(`${Player.NAMES[this.activePlayer]} következik. Dobáshoz nyomd meg a SPACE gombot.`);

        const diceRoll = (event) => {
            if (event.keyCode === 32) {

                Die.dice.forEach(d => d.moving = true);
                setTimeout(function () {
                    Die.dice.forEach(d => d.moving = false);
                    setTimeout(function () {
                        Die.dice[0].moveTo("DIE_MOVING", Die.diceStation);
                        Die.dice[1].moveTo("DIE_MOVING", Die.diceStation);
                        GameMechanic.step_3();
                    }, 1000);
                }, 3000);
            }
            document.removeEventListener('keydown', diceRoll);
        }
        document.addEventListener('keydown', diceRoll);
    }

    static step_3() {
        const player = this.players[this.activePlayer - 1];
        player.moveToField(player.currentField + Die.sum());
        this.activePlayer++;
        if (this.activePlayer > this.numberOfPlayers) {
            this.activePlayer = 1;
        }
        this.step_2();
    }
}